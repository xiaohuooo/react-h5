
import zhCNLocale from "./modules/zhCN.json";
import enUSLocale from "./modules/enUS.json";
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
// @ts-ignore
i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        debug: true,
        fallbackLng: 'en',
        interpolation: {
            escapeValue: false,
        },
        resources: {
            en: {
                translation: enUSLocale
            },
            zh: {
                translation: zhCNLocale
            }
        }
    });

export default i18n;