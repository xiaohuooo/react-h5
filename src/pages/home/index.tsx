import { Button } from 'antd'
import { useTranslation, Trans } from 'react-i18next';
import { Select } from 'antd';
import { getUserInfo } from '@/apis/login'
import { useEffect } from 'react';
const options = [
  { value: 'en', label: 'English' },
  { value: 'zh', label: '中文' },
]
const Home = () => {
  const navigate = useNavigate()
  const { t, i18n } = useTranslation();
  const getUser = async () => {
    const userInfo = await getUserInfo()
    console.log(userInfo);
    
  }
  useEffect(() => {
    getUser()
  })

  return (
    <>
      <Select options={options} onChange={(value) => {
        i18n.changeLanguage(value)
      }} />
      <Button type="primary" onClick={() => navigate('/home-detail')}>
        navigate to 404
      </Button>
      {/* <Button danger onClick={() => navigate('/inspection-detail')}>
        navigate to inspection-detail
      </Button> */}
      <Button type="primary" onClick={() => navigate('/login')}>
        navigate to login
      </Button>
    </>
  )
}

export default Home
