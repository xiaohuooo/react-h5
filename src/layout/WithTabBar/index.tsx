import React, { FC, Suspense } from 'react'
import useTabBarStore from '@/store/useTabBarStore'
import WithLocationListener from '@/routes/RouteGuard'
import { Image, Layout, Tabs } from 'antd'
import { Content, Footer } from 'antd/es/layout/layout'
import type { TabBarList } from '@/store/useTabBarStore/config'
import { useTranslation, Trans } from 'react-i18next'
// @ts-ignore
import { CSSTransition, SwitchTransition } from 'react-transition-group'
import './index.scss'
import routes from '@/routes/modules'
interface Config extends TabBarList {
  currentTab: string
}
export interface TabProps {
  config: Config
}
const TabLabel: FC<TabProps> = (props: TabProps) => {
  const { iconPath, text, selectedIconPath, pagePath, currentTab } =
    props.config

  const isCurrentTab = () => currentTab === pagePath
  const { t } = useTranslation()
  return (
    <div>
      <Image
        src={isCurrentTab() ? selectedIconPath : iconPath}
        width={30}
        preview={false}
      />
      <div className={isCurrentTab() ? 'text-[#42A2FA]' : 'text-[#B8B8D2]'}>
        {t(`menu.${text}`)}
      </div>
    </div>
  )
}

const TabBar = () => {
  const negative = useNavigate()

  const { currentTab, config, switchTab, confirmNavigate } = useTabBarStore(
    state => ({
      currentTab: state.currentTab,
      config: state.config,
      switchTab: state.switchTab,
      confirmNavigate: state.confirmIsShowAfterNavigate
    })
  )

  const location = useLocation()
  useMount(() => confirmNavigate(location))

  const handleTabChange = (activeTab: string) => {
    negative(activeTab)
    switchTab(activeTab)
  }

  return (
    <Tabs
      activeKey={currentTab}
      items={config.list.map(item => {
        return {
          key: item.pagePath,
          label: <TabLabel config={{ ...item, currentTab }} />
        }
      })}
      tabPosition="bottom"
      onChange={handleTabChange}
    />
  )
}

// interface LayoutWithTabBarProps {
//   children: React.ReactNode
// }
const LayoutWithTabBar: FC<LayoutWithTabBarProps> = (
  props: LayoutWithTabBarProps
) => {
  // const { children } = props

  const { contentStyle, setMarginBottom, isShow } = useTabBarStore(state => ({
    isShow: state.isShow,
    contentStyle: state.contentStyle,
    setMarginBottom: state.setMarginBottom
  }))

  const footerRef = useRef(null)
  const size = useSize(footerRef)
  useEffect(() => {
    setMarginBottom((size?.height as number) ?? 0)
  }, [size, isShow])
  const location = useLocation()
  const currentOutlet = useOutlet()
  const { nodeRef } =
    routes.find((route: any) => route.path === location.pathname) ?? {}
  return (
    <Layout className="custom-layout">
      <Content className="custom-layout-content" style={contentStyle}>
        <WithLocationListener>
          <SwitchTransition>
            <CSSTransition
              key={location.pathname}
              nodeRef={nodeRef}
              timeout={300}
              classNames="page"
              unmountOnExit
            >
              <Suspense>
                <div ref={nodeRef} className="page">
                  {currentOutlet}
                </div>
              </Suspense>
            </CSSTransition>
          </SwitchTransition>
        </WithLocationListener>
      </Content>
      {useMemo(
        () => (
          <Footer
            ref={footerRef}
            className={mergeClassName([
              'custom-layout-footer',
              isShow ? 'flex' : 'hidden'
            ])}
          >
            <TabBar />
          </Footer>
        ),
        [isShow]
      )}
    </Layout>
  )
}

export default LayoutWithTabBar
