import type { RouteObject } from 'react-router-dom'
import { createRef } from 'react'
import { createBrowserRouter } from 'react-router-dom'
const Home = lazy(() => import('@/pages/home'))
const Inspection = lazy(() => import('@/pages/inspection'))
const Book = lazy(() => import('@/pages/book'))
const Mine = lazy(() => import('@/pages/mine'))
const Error = lazy(() => import('@/pages/error'))
const InspectionDetail = lazy(() => import('@/pages/inspection/detail'))
const Login = lazy(() => import('@/pages/login'))

const UnDefPage = lazy(() => import('@/pages/404'))
const LayoutWithTabBar = lazy(() => import('@/layout/WithTabBar'))
const routesConfig: any = [
  // {
  //   path: '/',
  //   element: <Navigate to={'/home'} />,
  //   nodeRef: createRef()
  // },
  {
    path: '/',
    name: 'Home',
    element: <Home />,
    nodeRef: createRef()
  },
  {
    path: '/book',
    element: <Book />,
    nodeRef: createRef()
  },
  {
    path: '/inspection',
    element: <Inspection />,
    nodeRef: createRef()
  },
  {
    path: '/inspection-detail',
    element: <InspectionDetail />,
    nodeRef: createRef()
  },
  {
    path: '/error',
    element: <Error />,
    nodeRef: createRef()
  },
  {
    path: '/mine',
    element: <Mine />,
    nodeRef: createRef()
  },
  {
    path: '/login',
    element: <Login />,
    nodeRef: createRef()
  },
  {
    path: '/404',
    element: <UnDefPage />,
    nodeRef: createRef()
  },
  {
    path: '*',
    element: <Navigate to={'/404'} />,
    nodeRef: createRef()
  }
]

export const router = createBrowserRouter([
  {
    path: '/',
    element: <LayoutWithTabBar />,
    children: routesConfig.map(route => ({
      index: route.path === '/',
      path: route.path === '/' ? undefined : route.path,
      element: route.element
    }))
  }
])

export default routesConfig
