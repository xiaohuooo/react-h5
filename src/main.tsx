import React, { Suspense, useEffect, useState } from 'react'
import ReactDOM from 'react-dom/client'
// import Routes from './routes'
import { router } from './routes/modules'
import LayoutWithTabBar from './layout/WithTabBar'
import { BrowserRouter } from 'react-router-dom'
import { ConfigProvider } from 'antd'
import { RouterProvider } from 'react-router-dom'
import './i18n'
import './index.css'
import 'uno.css'
ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <ConfigProvider>
    <RouterProvider router={router} />
  </ConfigProvider>
)
