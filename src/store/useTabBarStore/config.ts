import Home from '@/assets/tabImgs/home.png'
import Inspection from '@/assets/tabImgs/inspection.png'
import Book from '@/assets/tabImgs/book.png'
import Error from '@/assets/tabImgs/error.png'
import Mine from '@/assets/tabImgs/mine.png'
import UnHome from '@/assets/tabImgs/un-home.png'
import UnInspection from '@/assets/tabImgs/un-inspection.png'
import UnBook from '@/assets/tabImgs/un-book.png'
import UnError from '@/assets/tabImgs/un-error.png'
import UnMine from '@/assets/tabImgs/un-mine.png'

export interface TabBarList {
  pagePath: string
  selectedIconPath: string
  iconPath: string
  text: string
}
export interface TabBar {
  color: string
  selectedColor: string
  list: TabBarList[]
}

const tabBarList: TabBar = {
  color: '#B8B8D2',
  selectedColor: '#42A2FA',
  list: [
    {
      pagePath: '/',
      selectedIconPath: Home,
      iconPath: UnHome,
      text: 'home'
    },
    {
      pagePath: '/inspection',
      selectedIconPath: Inspection,
      iconPath: UnInspection,
      text: 'inspection'
    },
    {
      pagePath: '/book',
      selectedIconPath: Book,
      iconPath: UnBook,
      text: 'book'
    },
    {
      pagePath: '/error',
      selectedIconPath: Error,
      iconPath: UnError,
      text: 'error'
    },
    {
      pagePath: '/mine',
      selectedIconPath: Mine,
      iconPath: UnMine,
      text: 'mine'
    }
  ]
}

export default tabBarList
