import { getUserInfo } from '@/apis/login'
import { create } from 'zustand'
import { persist } from 'zustand/middleware'

interface UseUserStore {
  token: string
  userInfo: Record<string, any>
  setToken: (token: string) => void
  setUserInfo: () => Promise<void>
}
const initStoreVal = {
  token: '',
  userInfo: {}
}
const store = (set: any, get: any) => ({
  ...initStoreVal,
  setToken: (token: string) => {
    set({ token })
  },
  setUserInfo: async () => {
    const userInfo = await getUserInfo()
    set({ userInfo })
  }
})

const persistStore = persist(store, { name: 'user_info' })
const useUserStore = create<UseUserStore>()(persistStore)

export default useUserStore
