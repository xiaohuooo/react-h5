import type { RequestLoginForm } from './types'

export const login = async (data: RequestLoginForm) => {
  const loginResult = await request.post<string>({
    url: '/api/system/wechat/login',
    data
  })
  return loginResult.data
}

export const getUserInfo = async () => {
  const userInfoResult = await request.get({
    url: '/api/system/getLoginUser'
  })
  return userInfoResult.data
}
