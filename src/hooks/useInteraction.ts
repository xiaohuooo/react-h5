import { message } from 'antd'
import type { ArgsProps } from 'antd/es/message'

const defaultOptions: Partial<ArgsProps> = {
  duration: 2,
  type: 'success'
}

/**
 * @description 默认状态为success
 */
export const useMessage = (options: ArgsProps) => {
  message.open({ ...defaultOptions, ...options })
}
