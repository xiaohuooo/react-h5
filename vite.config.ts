import react from '@vitejs/plugin-react-swc'
import AutoImport from 'unplugin-auto-import/vite'
import UnoCSS from 'unocss/vite'
import postcsspxtoviewport8plugin from 'postcss-px-to-viewport-8-plugin'
import { defineConfig } from 'vite'
import { loadEnv, createProxy } from './src/utils/dotenv'
import { resolve } from 'path'
import { viteMockServe } from 'vite-plugin-mock'
const { VITE_APP_API_BASE_URL } = loadEnv(process.env.NODE_ENV)
const isDevelopment = process.env.NODE_ENV === 'development';
// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 4396,
    host: '0.0.0.0',
    open: false,
    cors: true,
    proxy: createProxy(VITE_APP_API_BASE_URL, '/api')
  },
  plugins: [
    react(),
    AutoImport({
      include: [/.[tj]sx?$/],
      imports: ['react', 'react-router-dom', 'ahooks'],
      dirs: [
        './src/utils',
        './src/utils/request',
        './src/hooks',
        './src/store/useTabBarStore',
        './src/config',
        './src/apis'
      ],

      dts: true,
      defaultExportByFilename: false
    }),
    isDevelopment && viteMockServe({
      mockPath: 'mock', //mock文件地址
      enable: true,
      watchFiles: true,
      logger: true, //是否在控制台显示请求日志
    }),
    UnoCSS()
  ],
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
      '#': resolve(__dirname, './src/typings')
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@import "@/styles/var.scss";`
      }
    },
    postcss: {
      plugins: [
        postcsspxtoviewport8plugin({
          unitToConvert: 'px',
          viewportWidth: 375,
          unitPrecision: 5,
          propList: ['*'],
          viewportUnit: 'vw',
          fontViewportUnit: 'vw',
          selectorBlackList: [],
          minPixelValue: 1,
          mediaQuery: false,
          replace: true,
          exclude: undefined,
          landscape: false,
          landscapeUnit: 'vw'
        })
      ]
    }
  }
})
