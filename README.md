#### 介绍
> 使用 React+Vite
> 
> 集成了zustand，使状态管理更加清晰简便
> 
> 集成了unocss，不再需要为少量的样式而创建CSS类

#### 软件架构
> vite 4.1.0
> 
> React 18.2.0
> 
> react-router-dom 6.8.1
> 
> zustand 4.3.2
> 
> antd 5.1.7
> 
> axios 1.3.2
> 
> unocss 0.49.4
> 
> ahooks 3.7.4